package com.kb24.mycustomview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import org.jetbrains.annotations.NotNull;

public class CustomStarView extends View {

    private float outerCircle;
    private float innerCircle;

    private Paint outerCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint innerCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint starPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private Path starPath = new Path();

    private int mCount;
    private int startAngle;

    private int centerX;
    private int centerY;

    public CustomStarView(Context context) {
        super(context);
        initView(context, null, 0, 0);
    }

    public CustomStarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs, 0, 0);
    }

    public CustomStarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomStarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initView(@NotNull Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomStarView, defStyleAttr, defStyleRes);
        outerCircle = a.getDimension(R.styleable.CustomStarView_outerCircleRadius, getResources().getDimension(R.dimen.star_view_default_outer_circle_radius));
        innerCircle = a.getDimension(R.styleable.CustomStarView_innerCircleRadius, getResources().getDimension(R.dimen.star_view_default_inner_circle_radius));
        mCount = a.getInt(R.styleable.CustomStarView_count, getResources().getInteger(R.integer.star_view_default_count));
        startAngle = a.getInt(R.styleable.CustomStarView_startAngle, getResources().getInteger(R.integer.star_view_default_start_angle));

        int innerCircleColor = a.getColor(R.styleable.CustomStarView_innerCircleColor, ContextCompat.getColor(context, R.color.colorPrimary));
        int outerCircleColor = a.getColor(R.styleable.CustomStarView_outerCircleColor, ContextCompat.getColor(context, R.color.colorPrimaryDark));
        int starColor = a.getColor(R.styleable.CustomStarView_starColor, ContextCompat.getColor(context, R.color.colorAccent));
        a.recycle();

        outerCirclePaint.setColor(outerCircleColor);
        outerCirclePaint.setStyle(Paint.Style.FILL);

        innerCirclePaint.setColor(innerCircleColor);
        innerCirclePaint.setStyle(Paint.Style.FILL);

        starPaint.setColor(starColor);
        starPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        /*super.onMeasure(widthMeasureSpec, heightMeasureSpec);*/

        int desiredWidth = (int) getResources().getDimension(R.dimen.star_view_default_size);
        int desiredHeight = (int) getResources().getDimension(R.dimen.star_view_default_size);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        width += getPaddingLeft() + getPaddingRight();
        height += getPaddingTop() + getPaddingBottom();
        //MUST CALL THIS
        centerX = ((width - getPaddingRight()) / 2) + (getPaddingLeft() / 2);
        centerY = ((height - getPaddingBottom()) / 2) + (getPaddingTop() / 2);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        starPath.reset();

        float angleStep = 360f / (mCount * 2);
        double firstAngle = Math.toRadians(startAngle);
        starPath.moveTo((float) (centerX + (outerCircle * Math.cos(firstAngle))), (float) (centerY + (outerCircle * Math.sin(firstAngle))));

        for (int i = 1; i < mCount * 2; i++) {
            double angle = Math.toRadians(angleStep * i + startAngle);
            float x;
            float y;
            if (i % 2 == 1) {
                x = (float) (centerX + (innerCircle * Math.cos(angle)));
                y = (float) (centerY + (innerCircle * Math.sin(angle)));
            } else {
                x = (float) (centerX + (outerCircle * Math.cos(angle)));
                y = (float) (centerY + (outerCircle * Math.sin(angle)));
            }
            starPath.lineTo(x, y);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(centerX, centerY, outerCircle, outerCirclePaint);
        canvas.drawPath(starPath, starPaint);
        canvas.drawCircle(centerX, centerY, innerCircle, innerCirclePaint);
    }
}
